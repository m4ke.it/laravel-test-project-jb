<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laravel PHP Framework</title>
</head>
<body>

	@yield('content')
	@yield('forms')
	@yield('admin')

	@if(Auth::check())

	{{   link_to("logout", "logout") }}
	@else
	{{   link_to("login", "login") }}



	@endif
	{{ Session::get('message') }}

	
</body>
</html>