@extends('layout')



@section('content')

<h1> All Users </h1>
<table>
	<thead>

		<td>id </td><td>first Name </td> <td> Last name </td><td>  email address </td>  

	</thead>
<tbody>

@foreach ($users as $user)
<tr>
	<td> {{ $user->id}}</td><td>{{ $user->fname}} </td> 
	<td> {{ $user->lname}}</td><td> {{ $user->email}}</td> 
	<td>{{Form::open(array('action' => array('UserController@destroy', $user->id), 'method' => 'DELETE')) }}


		{{ Form::submit('DELETE') }}

		{{Form::close()}}


	</td> 
	<td>{{link_to_route('user.edit', "EDIT USER", $parameters = $user->id ) }}</td> 
	
</tr>

@endforeach
</tbody>
</table>





@stop