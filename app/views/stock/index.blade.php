@extends('layout')


@section('content')

<h1> wellcome {{$user->fname}} {{$user->lname}}</h1>
@if( ! $baskets->isEmpty())


@foreach($baskets as $basket)

<li>basket n° {{$basket->id }} 
	{{--*/  $id=$basket->id; /*--}}
	{{link_to_route('stock.show', "Show", $id ) }}

	{{Form::model($basket, array('action' => array("StockController@destroy", $basket->id), 'method' => 'DELETE')) }}
	{{ Form::submit('Delete this basket') }}
	{{Form::close()}}
 
	
	<table>
		<thead>

		<td>id </td><td>color </td> <td> type </td><td>  weight </td>  

		</thead>
		<tbody>
	@foreach($basket->fruit as $fruit)
	
			<tr>
				<td>{{$fruit->id}} </td><td>{{$fruit->color}}</td> 
				<td>{{$fruit->type}}</td><td>{{$fruit->weight}}</td>
				<td>{{link_to_route('fruit.edit', "Edit Fruit", $fruit->id ) }} </td>
				<td>{{Form::model($fruit, array('action' => array("FruitsController@destroy", $fruit->id), 'method' => 'DELETE')) }}
					{{ Form::submit('Delete fruit') }}
					{{Form::close()}}
				</td>
			</tr>


	@endforeach
		</tbody>
	</table></li>


<br>


@endforeach
@endif

	{{--*/  $bask = new Basket   /*--}}
	{{Form::model($bask, array('action' => array("StockController@store", $user->id), 'method' => 'POST')) }}
	{{ Form::submit('Add basket') }}
	{{Form::close()}}



@stop