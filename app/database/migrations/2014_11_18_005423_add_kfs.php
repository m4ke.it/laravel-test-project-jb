<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKfs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('baskets', function(Blueprint $table)
		{
			$table->integer('user_id');
		});
		Schema::table('fruits', function(Blueprint $table)
		{
			$table->integer('basket_id');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fruits', function(Blueprint $table)
		{
			$table->dropColumn('basket_id');
		});
		Schema::table('baskets', function(Blueprint $table)
		{
			$table->dropColumn('user_id');
			
		});
		//
	}

}
