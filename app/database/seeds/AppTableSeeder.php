<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AppTableSeeder extends Seeder {

	public function run()
	{


		//clears DBs

		DB::table('users')->delete();
		

		//Seed

		$userAdmin = User::create(array(

				'email' 	=>	'admin@grocery.com',
				'lname' 	=>	'dsh',
				'password' 	=> 	Hash::make('1234'),
				'fname' 	=>	 'jb',
				'admin'		=> 	1,
				'merchant'	=> 	0

			));
		$userMerchant = User::create(array(

				'email' 	=>	'merchant@grocery.com',
				'lname' 	=>	'Merchant',
				'password' 	=> 	Hash::make('12345'),
				'fname' 	=>	 'first',
				'admin'		=> 	0,
				'merchant'	=> 	1

			));
		$userClient = User::create(array(

				'email' 	=>	'client@grocery.com',
				'lname' 	=>	'Client',
				'password' 	=> 	Hash::make('123456'),
				'fname' 	=>	 'last',
				'admin'		=> 	0,
				'merchant'	=> 	0

			));

		$this->command->info('Users,...OK!');



		//$faker = Faker::create();
		/*$fakeFruits = array('banana', 'apple', 'pear', 'paenaple', 'grape');
		$fakeColor = array('red', 'green', 'yellow', 'black');
		$faketab = array(25, 50, 75);

		for($i=0; $i<20, $i++))
		{
			Fruit::create([

			]);*/
		
	}

}