<?php

class Basket extends \Eloquent {
	protected $fillable = ['user_id'];









	public function fruit()
    {
        return $this->hasMany('Fruit');

    }


    public function user()
    {
        return $this->belongsTo('User','user_id');
    }



    public function deleteFruits()
    {
        $this->fruit()->delete();


    }


    public function addBasket()
    {
    	if (Auth::check() && (Auth::user()->admin) == 0)
    	{
    		$basket = new Basket();
	    	$user = Auth::user();
   		 	$user->basket()->save($basket);
    		return true;
    	}
    	else return flase;
    	

    }
}

