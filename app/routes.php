<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');


Route::get('/login', 'SessionsController@create');


Route::resource('user', 'UserController');

Route::get('logout', 'SessionsController@logout' );


route::post('active', 'SessionsController@activeUser');

//fruits
Route::post('fruit', array('before' => 'storeFruit|checkMerchant', 'uses' => 'FruitsController@store'));
Route::put('fruit/{fruit}', array('before' => 'storeFruit|updDelFruit|checkMerchant', 'uses' => 'FruitsController@update'));
Route::post('fruit/{fruit}', array('before' => 'updDelFruit|checkMerchant', 'uses' => 'FruitsController@destroy'));




//stock, baskets
Route::get('stock', array('before' => 'checkMerchant', 'uses' => 'StockController@index'));
Route::post('stock', array('before' => 'checkMerchant', 'uses' => 'StockController@store'));
Route::get('stock/{id}', array('before' => 'checkMerchant|updDelBasket', 'uses' => 'StockController@show'));
Route::post('stock/{id}', array('before' => 'checkMerchant|updDelBasket', 'uses' => 'StockController@destroy'));


//Shops, HomeCotroller

Route::get('shop/fruits', 'HomeController@fruitsAll');
Route::post('shop/fruits', 'HomeController@getFruitsBy');



Route::get('shop/baskets', 'HomeController@getBaskets');
Route::post('shop/baskets', 'HomeController@getBasketsBy');




Route::post('/connection', 'SessionsController@connection');
	