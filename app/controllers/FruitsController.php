<?php

class FruitsController extends \BaseController {




	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$fruits = Fruit::all();
		return Response::json(['data'=>$fruits]);

	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$fruit = new Fruit;
		
		foreach($fruit->f as $value)
		{
			$array[$value]=Input::get($value);
		}

		$fruit->create($array);
		return Response::json(["error"=>false,
								"data"=>$fruit,
								"message"=>"Fruit ".$fruit->type." added",
								]);

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
		
		$fruit = Fruit::find($id);
		$update = false;
		$update = $fruit->update(Input::all());
		if($update)
		{
			return Response::json(['error'=>false,
								"message"=>"Fruit updated",
								"data"=>$fruit

										]);
		}

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

		
		$fruit = Fruit::find($id);
		$fruit->delete($id);
		return Response::json(['error'=>false,
								"message"=>"Fruit deleted",
								"data"=>$fruit

										]);
		
		



	}


}
