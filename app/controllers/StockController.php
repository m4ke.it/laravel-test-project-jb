<?php

class StockController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$array = array();
		$baskets = Basket::where('user_id', '=', Auth::user()->id)->get();
		foreach($baskets as $basket)
		{

			$fruits = Fruit::where('basket_id','=', $basket->id)->get();
			$array[$basket->id] = $fruits;

		}
		return Response::json(['error'=>false,
								"data"=>$array
			]);
	}


	

	/**
	 * Store a newly created resource in storage.
	 * @param int $user->id
	 * @return Response
	 */
	public function store()
	{
		$basket = new Basket;
		$basket->user_id = Auth::user()->id;
		$basket->save();
		return Response::json(["error"=> false,
								"message"=>"basket added",
								"data"=> $basket]);
 


	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{	

		$fruits = Fruit::where('basket_id','=', $id)->get();
		$basket = Basket::find($id);
		return Response::json(["error"=>false, 
								"Message"=> "Basket n°".$basket->id,
								"data"=>
								$fruits
								//"data1"=>
			]);
	}



	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$basket = Basket::find($id);
		$basket->deleteFruits();
		$deleted = $basket->delete();
		if($deleted)
		{
			return Response::json(["error"=>false, 
								"Message"=> "Basket n°".$basket->id. "deleted",
								
			]);
		}		
	}


	























	


}
