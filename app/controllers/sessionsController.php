<?php


class SessionsController extends \BaseController {




	/**
	 * connection utilisateur.
	 * POST /sessions
	 *
	 * @return Response
	 */
	public function connection()
	{
		
    // The user is logged in...


		if (Auth::attempt(['email' => Input::get('email'),
		 'password' => Input::get('password')]))
        {
        	if(Auth::user()->active < 1)
        	{
        		return Response::json([
					  "error"=>true,
					   "code"=>"authorization",
					"message"=>[
							"accés refusé",
							"compte inactif"

							]
			]);


        	}
				
			return Response::json(
			[
					  "error"=>false,
					  "data"=>Auth::user(),
					"message"=>["Wellcome"]
			]);
		}
		else return Response::json([
					  "error"=>true,
					   "code"=>"authorization",
					"message"=>[
							"accés refusé"]
			]);
	}

	/**
	 * active le compte desactivé.
	 * GET /sessions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function activeUser()
	{
		if(Auth::check() && Auth::user()->admin >0)
		{	$id=Input::get('id');
			$user = User::find($id);
			$user->update(array('active'=>1));
				
			return Response::json(
			[
					  "error"=>false,
					   "code"=>"authorization",
					"message"=>["User n°:".$id."active"]
			]);
		}
		else
		{
			return Response::json([
					  "error"=>true,
					   "code"=>"authorization",
					"message"=>[
							"accés refusé"]
			]);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /sessions/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function descativeUser($id)
	{
		if(Auth::check() && Auth::user()->admin >0)
		{	
			$user = User::find($id)->get();
			$user->update(array('active'=>1));
				
			return Response::json(
			[
					  "error"=>false,
					   "code"=>"authorization",
					"message"=>["accepted","User n°:".$id."inactive"]
			]);
		}
		else
		{
			return Response::json([
					  "error"=>true,
					   "code"=>"authorization",
					"message"=>[
							"accés refusé"]
			]);
		}	}

	/**
	 * Update the specified resource in storage.
	 * PUT /sessions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /sessions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function logout()
	{
		if(Auth::check()){


			Auth::logout();
			return Response::json([
						  "error"=>false,
						   "code"=>"deconection",
						"message"=>[
								"A bientot!"]
				]);
		}
		else return Response::json([
						  "error"=>true,
						  "message"=>[
								"Vous n'etes pas connecté!"]
				]);
	}

}