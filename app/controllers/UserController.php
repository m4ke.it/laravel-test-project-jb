<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::all();
		return Response::json(["data"=>$users]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = array();
		$user = new User;
		if(Auth::check() && Auth::user()->admin >0)
		{
			if(null ==Input::get('admin'))
			{
				Input::merge(['admin' => 0]);
			}
			$array= array();
			foreach ($user->fillableFields as $key) {
					$array[$key] = Input::get($key);
			}
			if(Input::get('active') == null )
			{
				$array['active'] = 0;
			}

			$input['email'] = Input::get('email');

			$rules = array('email' => 'unique:users,email');
			$validator = Validator::make($input, $rules);
			if ($validator->fails()) {
				return Response::json(["error"=>true,
										"code"=>"??",
										"message"=>"email already used!"])   ;

			    
			}
			else {
			    $password= Hash::make(Input::get('password'));
				$user = User::create($array);
				$update = $user->update(array('password'=>$password));
				return Response::json(["error"=>false,
										"data"=>$user,
										"message  "=>$user->lname." created"])   ;	

			}
		}
		 else {
			return Response::json(array(
				"error"=>true,
				"code"=>"authorization",
				"message"=>"Vous n'avez pas les droits nécessaires pour créer un utilisateur, il faut être admin."
			));
		}	

		
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if(null !== $id)
		{
			$user = User::find($id);
			return Response::json(['data'=>$user]);	
		}
		else
		{
			return Response::json(['error'=> true,
									]);
		}

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{	
		if($id ==null)
		{
			return Response::json(["error"=>true, 
									"succes"=> false,
									"message"=>"Pas d'user spécifié"]);
		}
		if(Auth::user()->admin >0)
		{

		
			$user = User::find($id);
			$update = $user->update(Input::all());
			if($update)
			{	
				$password= Hash::make(Input::get('password'));
				$update = $user->update(array('password'=>$password));
				return Response::json(['error'=> false, 
										'data'=> $user
											]);

			}
			return Response::json(["error"=>true,
									"code"=>"Unknown",
									]);
		}
		return Response::json(['error'=> true,
								'code'=>'unautorized(ortho a verifier)',
								"message"=>"Vous devez etre admin pour une mise a jour user"
								]);
			
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{	

		if($id == null)
		{
			return Response::json(["error"=> true,
										"code"=>"unspecified",
										"message"=>"Unspecified user"

				]);
		}
		$succes= false;
		if(Auth::user()->admin == 0)
		{
			return  Response::json(array(
				"error"=>true,
				"code"=>"authorization",
				"message"=>"Vous n'avez pas les droits nécessaires pour effacer un utilisateur, il faut être admin."
			));

		}
		if( Auth::user()->id == $id)
		{
				 return Response::json(['succes' => $succes,
				 						"message"=>'You cant delete yourself'
				 		]);
		}
		$user = User::find($id);

		$succes =$user->delete($id);	

		return Response::json(['succes'=> $succes]);
	}




}
